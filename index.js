window.onscroll = function() {scrollFunction()};

// function scrollFunction() {
//   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
//     document.querySelector(".navbar").style.top = "0";
//   } else {
//     document.querySelector(".navbar").style.top = "-50px";
//   }
// }


$('#year').text(new Date().getFullYear());

$('body').scrollspy({ target: '#main-nav' });

$('.navbar-collapse a').click(function(){
	$(".navbar-collapse").collapse('hide');
});

$('body').click(function(){
	$(".navbar-collapse").collapse('hide');
});

$("#main-nav a").on('click', function (event) {
    if (this.hash !== "") {
      event.preventDefault();
  
      const hash = this.hash;
  
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {
  
        window.location.hash = hash;
      });
    }
  });